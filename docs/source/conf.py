# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Métodos Matemáticos'
copyright = '2022, Ezequiel C. Siqueira'
author = 'Ezequiel C. Siqueira'
release = '1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

#extensions = ['sphinxcontrib.bibtex']
#bibtex_bibfiles = ['refs.bib']



templates_path = ['_templates']
exclude_patterns = []

language = 'pt_BR'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_css_files=['css/custom.css',]

numfig=True
math_numfig = True
numfig_secnum_depth = 2
math_eqref_format = "Eq. ({number})"
