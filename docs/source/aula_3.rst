A equação do calor
==================

*Referência: E. Kreyszig, Advanced Engineering Mathematics, 9th ed., Ch. 12, pg. 552-568*


:math:`\phantom{\text{pargraph}}` A próxima EDP que vamos considerar é a equação do calor, também chamada de equação de difusão e permite modelar tanto a difusão de partículas quanto a propagação do calor em um metal. A EDP é dada pela :eq:`eqcalor`:

.. math::
  :label: eqcalor

    \begin{aligned}
    \nabla^{2}u-\dfrac{1}{c^{2}}\dfrac{\partial u}{\partial t}=0.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` Na :eq:`eqcalor`, :math:`u(\vec{r},t)` é a temperatura na posição :math:`\vec{r}` e no instante :math:`t`. A constante :math:`c^{2}` é a chamada **difusividade*, definida por :math:`c^{2}=\kappa/\sigma\rho`, onde :math:`\kappa` é a condutividade térmica, :math:`\sigma` é o calor específico e :math:`\rho` é a densidade.

:math:`\phantom{\text{pargraph}}` Vamos considerar o caso mais simples de uma barra de comprimento :math:`L`, isolada termicamente ao longo de seu comprimento de maneira que o calor se propaga apenas ao longo da mesma. Assumindo que a barra está disposta ao longo da direção-:math:`x`, vamos assumir que suas extremidades estão a uma temperatura nula, assim, temos que :math:`u(0,t)=u(L,0)=0` (veja :numref:`fig1`).

.. _fig1:

.. figure:: figuras/figura1.png
   :scale: 100%
   :align: center

   Ilustração de uma barra de comprimento :math:`L` cujas extremidades estão a uma temperatura fixa.


:math:`\phantom{\text{paragraph}}` Além das duas condições de contorno definidas acima, precisamos de uma condição inicial para :math:`u(x,t)` visto que a temperatura também depende do tempo. Assim, vamos assumir que :math:`u(x,0)=f(x)`, onde :math:`f(x)` define a distribuição de temperaturas ao longo da barra.

:math:`\phantom{\text{paragraph}}` A equação do calor para descrever a variação da temperatura ao longo da barra é dada pela versão unidimensional da :eq:`eqcalor`:

.. math::
  :label: eqcalor1d

    \begin{aligned}
   \dfrac{\partial^{2} u}{\partial x^{2}} =\dfrac{1}{c^{2}}\dfrac{\partial u}{\partial t}.
    \end{aligned}

:math:`\phantom{\text{paragraph}}` Para resolver a equação acima, vamos adotar o método de separação de variáveis, de maneira similar ao que fizemos com a equação da onda. Assim, vamos escrever a solução :math:`u(x, t)` como um produto de duas funções de uma variável, deste modo, segue que:

.. math::

    \begin{aligned}
    u(x,t)=F(x)G(t)
    \end{aligned}

e, substituindo na :eq:`eqcalor1d`, vamos obter:

.. math::

    \begin{aligned}
  G(t)\dfrac{d^{2} F(x)}{dx^{2}} =\dfrac{1}{c^{2}}F(x)\dfrac{dG(t)}{dt}.
    \end{aligned}

onde já trocamos as derivadas parciais por derivadas totais visto que temos a derivada de uma função de uma variável. Dividindo ambos os membros por :math:`F(x)G(t)`, podemos escrever:

.. math::

    \begin{aligned}
  \dfrac{1}{F(x)}\dfrac{d^{2} F(x)}{dx^{2}} =\dfrac{1}{c^{2}G(t)}\dfrac{dG(t)}{dt}=\lambda,
    \end{aligned}

onde :math:`\lambda` é uma constante, visto que ambos os membros da equação só podem ser iguais se foram iguais a uma constante. Assim, a EDP se reduz à solução de duas EDOs:

.. math::
  :label: edosheat

    \begin{aligned}
    &\dfrac{d^{2} F(x)}{dx^{2}}-\lambda F(x)=0, \\
    &\dfrac{dG(t)}{dt}-\lambda c^{2}G(t)=0
    \end{aligned}


:math:`\phantom{\text{paragraph}}` A equação para :math:`F(x)` apresenta a mesma forma que encontramos na solução da equação da onda. Naquela ocasião, notamos que as duas condições de contorno só poderão ser satisfeitas caso :math:`\lambda<0`. Sendo assim, redefinimos a constante de modo que :math:`\lambda =-p^{2}`. Deste modo, a solução geral para a função :math:`F(x)` é dada por:

.. math::
  :label: solgeralFx

    \begin{aligned}
    F(x)=A\cos px +B\sin px,
    \end{aligned}

e aplicando as condições de contorno, obtemos:

.. math::
  :label: Fxcondcontorno

    \begin{aligned}
    F_{n}(x)=B_{n}\sin\left(\dfrac{n\pi x}{L}\right),
    \end{aligned}

onde deixamos explícito os valores da constante :math:`p`, a qual assume valores inteiros de maneira que :math:`p\rightarrow p_{n}=\dfrac{n\pi}{L}`. Notamos também que o procedimento adotado aqui e idêntico ao que foi utilizado quando aplicamos as condições de contorno para a equação de onda.


A equação para :math:`G(t)` é diferente porque se trata de uma EDO de primeira ordem. Podemos resolvê-la facilmente visto que é separável, assim, reescrevemos esta equação na forma:

.. math::

    \begin{aligned}
    \dfrac{dG}{G}=-p^{2}_{n}c^{2}dt,
    \end{aligned}

onde já fizemos a troca de :math:`\lambda` pelo seu valor obtido após a aplicação da condição de contorno sobre :math:`F(x)`.

:math:`\phantom{\text{paragraph}}` Integrando ambos os membros obtemos:

.. math::
   :label: Gtequation

    \begin{aligned}
    G(t)=C_{n}e^{-p^{2}_{n}c^{2}t},
    \end{aligned}

onde  :math:`C_{n}` é uma constante.



:math:`\phantom{\text{paragraph}}` Com a :eq:`Fxcondcontorno`  e :eq:`Gtequation`, podemos escrever a solução geral na forma:

.. math::
  :label: solgeralintermed1

    \begin{aligned}
    u_{n}(x,t)=B_{n}C_{n}e^{-p^{2}_{n}c^{2}t}\sin\left(\dfrac{n\pi x}{L}\right),
    \end{aligned}

e definindo uma nova constante :math:`A_{n}=C_{n}B_{n}`, podemos escrever a solução geral para o problema da equação do calor:

.. math::
  :label: solgeralcalor

    \begin{aligned}
    u(x,t)=\sum_{n=1}^{\infty}A_{n}e^{-p^{2}_{n}c^{2}t}\sin\left(\dfrac{n\pi x}{L}\right).
    \end{aligned}

:math:`\phantom{\text{paragraph}}` A constante :math:`A_{n}` pode ser determinada aplicando-se a condição inicial :math:`u(x,0)=f(x)`, onde :math:`f(x)` é uma função que nos dá a distribuição de temperatura ao longo da barra para o instante inicial :math:`t=0`. O procedimento é idêntico ao que foi realizado para o caso da equação da onda de modo que não iremos repetí-lo aqui. Em vez disso, mostramos a expressão final para a constante :math:`A_{n}`:

.. math::
  :label: constanteAn

    \begin{aligned}
    A_{n}=\dfrac{2}{L}\int_{0}^{L}f(x)\sin\left(\dfrac{n\pi x}{L}\right).
    \end{aligned}


Exemplo
-------
**1.** (E. Kreyzsig, 9th, Advanced Engineering Mathematics, pg. 555) *Encontre a temperatura* :math:`u(x,t)` *em uma barra de cobre lateralmente isolada com* :math:`L=80~\text{cm}` *se a temperatura inicial é* :math:`u(x,0)=T_{0}\sin(\pi x/L)` *com* :math:`T_{0}=100^{o}\text{C}` *e suas extremidades estão mantidas a* :math:`0 ^{o}\text{C}`. *Quanto tempo leva para a temperatura máxima na barra cair para* :math:`T=50^{o}\text{C}` *?*

*Dados do cobre:* :math:`\rho=8,92~\text{g/cm}^{3}`, :math:`c_{Cu}=0,092~\text{cal/g}^{o}\text{C}`, *e*  :math:`\kappa=0,95~\text{cal/cm}\cdot \text{s}\cdot ^{o}\text{C}`

.. centered:: Solução

:math:`\phantom{\text{paragraph}}` O coeficiente :math:`A_{n}` é dado por:

.. math::

    \begin{aligned}
    A_{n}=\dfrac{2}{L}\int_{0}^{L}T_{0}\sin\left(\dfrac{\pi x}{L}\right)\sin\left(\dfrac{n\pi x}{L}\right)~dx = T_{0}\delta_{n,1}
    \end{aligned}

onde lançamos mão da relação de ortogonalidade no último passo. A solução é então dada por:

.. math::

    \begin{aligned}
    u(x,t)=\sum_{n=1}^{\infty}T_{0}\delta_{n,1}e^{-p^{2}_{n}c^{2}t}\sin\left(\dfrac{n\pi x}{L}\right),
    \end{aligned}

o que nos permite escrever,

.. math::

    \begin{aligned}
    u(x,t)=T_{0}e^{-\pi^{2}c^{2}t/L^{2}}\sin\left(\dfrac{\pi x}{L}\right),
    \end{aligned}


:math:`\phantom{\text{paragraph}}` A posição na barra onde se registra a tempertura máxima é tal que a função :math:`\sin(\pi x/L)=1`. Assim, temos que:

.. math::

    \begin{aligned}
    u_{m}(x_{m},t)=T_{0}e^{-\pi^{2}c^{2}t/L^{2}},
    \end{aligned}

onde denotamos :math:`u_{m}` a temperatura máxima e :math:`x_{m}` a posição na barra para a qual a função seno é igual a 1.  De acordo com o enunciado,  precisamos determinar o instante de tempo em que a temperatura máxima cai para a metade de seu valor :math:`T_{0}`. Assim, substituindo :math:`u_{m}(x_{m},t)=T_{0}/2`, segue que:

.. math::

    \begin{aligned}
    \dfrac{T_{0}}{2}=T_{0}e^{-\pi^{2}c^{2}t/L^{2}},
    \end{aligned}

e, isolando o tempo na equação acima, segue que:

.. math::

    \begin{aligned}
    t=\dfrac{L^{2}}{\pi^{2}}\dfrac{\rho c_{\text{Cu}}}{\kappa}\ln 2,
    \end{aligned}

:math:`\phantom{\text{paragraph}}` Substituindo-se os valores dados no enunciado, obtemos :math:`t\sim 388~\text{s}`.
