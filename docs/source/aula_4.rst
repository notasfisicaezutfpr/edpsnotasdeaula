Equação de Laplace
==================

:math:`\phantom{\text{pargraph}}` Nos capítulos anteriores discutimos a equação da onda e a equação do calor. Neste capítulo vamos discutir a solução da equação de Laplace, que é uma EDP elíptica, tendo aplicações em termodinâmica e eletromagnetismo. A equação de Laplace é dada por:

.. math::
  :label: laplacedef

    \begin{aligned}
    \nabla^{2}u=0.
    \end{aligned}


:math:`\phantom{\text{pargraph}}` Comparando com a equação do calor, vemos que a :eq:`laplacedef` é a sua versão no estado estacionário, i.e., no equilíbrio termodinâmico, caracterizado pelo fato :math:`\partial u/\partial t=0`. De fato, a equação de Laplace é um caso onde não se verifica a dependência temporal.

:math:`\phantom{\text{pargraph}}` Vamos assumir que :math:`u(x,y)` represente a temperatura de uma placa de dimensões :math:`H\times L`.  Assim, em coordenadas retangulares, podemos reescrever a :eq:`laplacedef` na forma:

.. math::
  :label: laplacedef2

    \begin{aligned}
    \dfrac{\partial^{2} u}{\partial x^{2}}+\dfrac{\partial^{2} u}{\partial y^{2}}=0
    \end{aligned}

:math:`\phantom{\text{pargraph}}` A solução geral pode ser determinada por meio da técnica de separação de variáveis, assim, escrevemos :math:`u(x, y)=X(x)Y(y)` e, substituindo na :eq:`laplacedef2`, obtemos:

.. math::

    \begin{aligned}
    Y(y)\dfrac{d^{2} X(x)}{dx^{2}}+X(x)\dfrac{d^{2}Y(y)}{dy^{2}}=0,
    \end{aligned}
o que pode ser escrito na forma:

.. math::

    \begin{aligned}
    \dfrac{1}{X(x)}\dfrac{d^{2} X(x)}{dx^{2}}=-\dfrac{1}{Y(y)}\dfrac{d^{2}Y(y)}{dy^{2}}=-k^{2}
    \end{aligned}

e, da mesma forma que nos casos anteriores, ambos os membros da equação acima devem ser iguais a uma constante que definimos como sendo :math:`-k^{2}`. Esta escolha foi motivada pelas experiência obtida da solução das equações anteriores, onde notamos que as condições de contorno somente são satisfeitas para valores negativo das constantes. Disto seguem duas equações diferenciais ordinárias (EDOs), assim, escrevemos:

.. math::
   :label: EDosGerais

    \begin{aligned}
    \dfrac{d^{2}X(x)}{dx^{2}}+k^{2}X(x)&=0,\\
    \dfrac{d^{2}Y(y)}{dy^{2}}-k^{2}Y(y)&=0.
    \end{aligned}

:math:`\phantom{\text{paragraph}}` A solução das EDOs é bastante simples e fica de exercício para o leitor. Para :math:`k\neq 0`, temos:

.. math::
  :label: kdifferentzero

    \begin{aligned}
    X_{k}(x)&=A_{k}\cos kx +B_{k}\sin kx,\\
    Y_{k}(y)&=C_{k}\cosh ky+D_{k}\sinh ky,
    \end{aligned}

enquanto para a solução para :math:`k=0`, é dada por:

.. math::
  :label: kigualzero

    \begin{aligned}
    X_{0}(x)&=A^{\prime}_{0}x+B^{\prime}_{0},\\
    Y_{0}(y)&=C^{\prime}_{0}y+D^{\prime}_{0}.
    \end{aligned}

:math:`\phantom{\text{paragraph}}` A solução geral pode ser escrita na forma:

.. math::
  :label: solgeral

    \begin{multline}
   u(x,y)=A_{0}+B_{0}x+C_{0}y+D_{0}xy\\+\sum_{k\neq 0}[A_{k}\cos kx +B_{k}\sin kx+C_{k}\cosh ky+D_{k}\sinh ky],
    \end{multline}

onde redefinimos as constantes da :eq:`kigualzero` para expressar a :eq:`solgeral`.

Exemplo
-------
**1.** (E. Kreyzsig, 9th, Advanced Engineering Mathematics, pg. 555) *Encontre a expressão para a distribuição de temperatura para uma placa de dimensões* :math:`L\times H` *cuja temperatura em suas bordas é dada pela figura abaixo.*


.. _placa:

.. figure:: figuras/figLaplace.png
   :scale: 40%
   :align: center

   Ilustração de uma barra de comprimento :math:`L` cujas extremidades estão a uma temperatura fixa.


.. centered:: Solução

:math:`\phantom{\text{paragraph}}` Vamos aplicar as condições de contorno à solução da equação de Laplace, dada pela :eq:`solgeral`. Para isso, começamos com a condição :math:`u(0,y)=T_{0}`, assim, segue que:

.. math::
  :label: cond1

    \begin{aligned}
    u(0,y)=A_{0}+C_{0}y+\sum_{k\neq0}A_{k}(C_{k}\cosh ky+D_{k}\sinh ky)=T_{0},
    \end{aligned}

e, para esta equação ser satisfeita, devemos impor :math:`A_{k}=0,~\forall~k>0` e :math:`A_{0}=T_{0}`.


:math:`\phantom{\text{paragraph}}` Com isso, a solução geral passa a ser escrita na forma:

.. math::

    \begin{align}
   u(x,y)=T_{0}+B_{0}x+D_{0}xy+\sum_{k\neq 0}B_{k}\sin kx[C_{k}\cosh ky+D_{k}\sinh ky].
    \end{align}

:math:`\phantom{\text{paragraph}}` Vamos agora aplicar a segunda condição para :math:`x=L`, assim, segue que:

.. math::
 :label: geral1

   \begin{align}
  u(L,y)=T_{0}+B_{0}L+D_{0}Ly+\sum_{k\neq 0}B_{k}\sin kL[C_{k}\cosh ky+D_{k}\sinh ky]=T_{0}
   \end{align}

e, comparando ambos os membros da equação acima, podemos notar que: :math:`B_{0}=D_{0}=0`, :math:`k=k_{n}=n\pi/L`. Com isso, a solução geral pode ser escrita na forma:

.. math::
 :label: geral2

   \begin{align}
  u(x,y)=T_{0}+\sum_{n\neq 0}\sin k_{n}x[C_{n}\cosh k_{n}y+D_{n}\sinh k_{n}y].
   \end{align}

onde absorvemos a constante :math:`B_{k}` em :math:`C_{n}` e :math:`D_{n}`.

:math:`\phantom{\text{paragraph}}` Vamos agora aplicar as condições de contorno para :math:`y=0` e :math:`y=H`. Assim, fazendo :math:`y=0` na :eq:`geral2`, segue que:

.. math::

   \begin{align}
  u(x,0)=T_{0}+\sum_{n\neq 0}C_{n}\sin k_{n}x=T_{0}
   \end{align}

o que nos leva a concluir que :math:`C_{n}=0`. Assim, segue que:

.. math::
 :label: geral3

   \begin{align}
  u(x,y)=T_{0}+\sum_{n\neq 0}D_{n}\sin k_{n}x\sinh k_{n}y.
   \end{align}

:math:`\phantom{\text{paragraph}}` Finalmente, vamos consdiderar a condição de contorno para :math:`y=H`, assim, segue que:

.. math::

   \begin{align}
  u(x,H)=T_{0}+\sum_{n\neq 0}D_{n}\sin k_{n}x\sinh k_{n}H=0
   \end{align}

e substituindo a expressão para :math:`k_{n}`, podemos reescrever a equação acima na forma:

.. math::

   \begin{align}
  -T_{0}=\sum_{n\neq 0}D_{n}\sin\dfrac{n\pi x}{L}\sinh\dfrac{n\pi H}{L}.
   \end{align}

:math:`\phantom{\text{paragraph}}` Usando a ortogonalidade das funções seno, podemos escrever:

.. math::

   \begin{align}
  -T_{0}\dfrac{2}{L}\int_{0}^{L}\sin\dfrac{n\pi x}{L}~dx=\sum_{m\neq 0}D_{m}\sinh\dfrac{m\pi H}{L}\left(\dfrac{2}{L}\int_{0}^{L}\sin\dfrac{n\pi x}{L}\sin\dfrac{m\pi x}{L}~dx\right),
   \end{align}

ou ainda,

.. math::

   \begin{align}
  \dfrac{2T_{0}}{L}[\cos(n\pi)-1]=\dfrac{2T_{0}L}{n\pi L}[(-1)^{n}-1]=\sum_{m\neq 0}D_{m}\sinh\dfrac{m\pi H}{L}\delta_{mn}=D_{n}\sinh\dfrac{n\pi H}{L}.
   \end{align}

:math:`\phantom{\text{paragraph}}` O lado esquerdo da equação acima é diferente de zero somente quando :math:`n`  é um número ímpar. Neste caso, a constante :math:`D_{n}` pode ser expressa da seguinte maneira:

.. math::
 :label: constDn

   \begin{align}
 D_{n}=-\dfrac{4T_{0}}{n\pi}\dfrac{1}{\sinh\dfrac{n\pi H}{L}}, \qquad n=\text{ímpar}.
   \end{align}


:math:`\phantom{\text{paragraph}}` Substituindo :eq:`constDn` na  :eq:`geral3`, segue que:


.. math::
 :label: geral4

   \begin{align}
  u(x,y)=T_{0}-\dfrac{4T_{0}}{\pi}\sum_{n\neq 0}\dfrac{1}{n}\sin\left(\dfrac{n\pi x}{L}\right)\dfrac{\sinh\left(\dfrac{n\pi y}{L}\right)}{\sinh\left(\dfrac{n\pi H}{L}\right)}, \quad n=\text{ímpar}
   \end{align}


:math:`\phantom{\text{paragraph}}` A :eq:`geral4` satisfaz as quatro condições de contorno. Fazendo :math:`x=0` e :math:`y=0` é fácil verificar  que os termos dentro da soma são reduzidos a zero visto que tanto a função :math:`\sin x` quando a função :math:`\sinh x` são nulas em :math:`x=0`. Além disso, a função seno é zero para múltiplos de :math:`\pi` e, deste modo também é fácil ver que a condição de contorno :math:`u(L,y)=0` também é satisfeita. Já para :math:`u(x,H)`, a :eq:`geral4`  resulta em:

.. math::
 :label: geralcondcont

   \begin{align}
  u(x,H)=T_{0}-\dfrac{4T_{0}}{\pi}\sum_{n\neq 0\atop{\text{ímpar}}}\dfrac{1}{n}\sin\left(\dfrac{n\pi x}{L}\right)\dfrac{\sinh\left(\dfrac{n\pi H}{L}\right)}{\sinh\left(\dfrac{n\pi H}{L}\right)}=T_{0}-\dfrac{4T_{0}}{\pi}\sum_{{n\neq 0}\atop{\text{ímpar}}}\dfrac{1}{n}\sin\left(\dfrac{n\pi x}{L}\right)
   \end{align}

e, aqui precisamos usar um resultado bem conhecido da teoria de séries de Fourier que nos indica que:

.. math::

    \begin{aligned}
    \sum_{n\neq 0\atop{\text{ímpar}}}\dfrac{1}{n}\sin\left(\dfrac{n\pi x}{L}\right)=\dfrac{\pi}{4},\quad\text{com}\quad x\in [0, L],
    \end{aligned}

o que nos permite escrever :eq:`geralcondcont` na forma:
:

.. math::

   \begin{align}
  u(x,H)=T_{0}-\dfrac{4T_{0}}{\pi}\dfrac{\pi}{4}=0,
   \end{align}

em conformidade com a condição de contorno.
