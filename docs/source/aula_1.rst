Classificação de Equações Diferenciais Parciais
===============================================

:math:`\phantom{\text{paragr}}` Neste capítulo iniciamos o estudo das Equações Diferenciais Parciais (EDPs), equações que aparecem em diversos contextos da ciência e da engenharia. O primeiro passo deste estudo é a classificação das EDPs, algo que é útil para que possamos escolher o caminho correto para a determinação de sua solução.

:math:`\phantom{\text{paragr}}` Vamos considerar uma EDP geral, definida pela :eq:`chap1eq1`. Nesta equação, os coeficientes :math:`A`, :math:`B`, :math:`C`, :math:`D`, :math:`E`, :math:`F` são tomados como constantes enquanto :math:`G` pode ser uma função das variáveis independentes :math:`x` e :math:`y`. Quando :math:`G=0` a equação é chamada de homogênea. O objetivo é determinar a função :math:`u=u(x,y)`.

.. math::
  :label: chap1eq1

   \begin{aligned}
   A\dfrac{\partial^{2}u}{\partial x^{2}}+B\dfrac{\partial^{2}u}{\partial x\partial y}+C\dfrac{\partial^{2}u}{\partial y^{2}}+D\dfrac{\partial u}{\partial x}+E\dfrac{\partial u}{\partial y}+Fu=G.
   \end{aligned}

:math:`\phantom{\text{paragr}}` A relação entre os coeficientes da :eq:`chap1eq1` permite classificar a EDP como *hiperbólica* se :math:`B^{2}-4AC>0`, *parabólica* se :math:`B^{2}-4AC=0`, e *elíptica* se :math:`B^{2}-4AC<0`. Quando os coeficientes são dependentes das coordenadas, então a classificação como parabólica, hiperbólica e elíptica são definidas apenas localmente, ou seja, a EDP pode assumir diferentes classificações no plano-:math:`xy` dependendo do valores relativos de :math:`A`, :math:`B` e :math:`C`. No entanto, para simplificar a discussão, vamos tratar do caso mais simples onde os coeficientes são constantes.


Notação das derivadas
**********************

:math:`\phantom{\text{paragr}}` Antes de prosseguir, vamos adotar uma notação mais simples para denotar as derivadas parciais. As derivadas da função :math:`u` podem ser escritas na forma:

.. math::
  :label: notacao

    \begin{aligned}
    \dfrac{\partial u}{\partial x}=u_{x}, \qquad \dfrac{\partial u}{\partial y}=u_{y}.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Podemos simplificar ainda mais a notação definindo índices genéricos :math:`i, j, k, \cdots`, etc.,  tal que :math:`u_{i}` pode ser igual a  :math:`u_{x}` ou :math:`u_{y}` dependendo se :math:`i=1` ou :math:`i=2`, respectivamente. Com esta notação, a :eq:`chap1eq1` pode ser escrita na forma:

.. math::
  :label: chap1eq2

    \begin{aligned}
    Au_{xx}+Bu_{xy}+Cu_{yy}+Du_{x}+Eu_{y}+Fu=G.
    \end{aligned}




Troca de variáveis na EDP
*************************

:math:`\phantom{\text{paragr}}` A :eq:`chap1eq2` é complicada por causa das derivadas de segunda ordem, ou seja, dos três primeiros termos com coeficientes :math:`A`, :math:`B` e :math:`C`. É possível eliminar algumas destas derivadas por meio de uma transformação de variáveis e, após esta transformação, teremos novos coeficientes que poderão ser igualados a zero levando às condições do parágrafo anterior sobre os coeficientes  :math:`A`, :math:`B` e :math:`C`.

Esta transformação consiste em definir novas variáveis :math:`\xi` e :math:`\eta` tal que :math:`\xi=\xi(x, y)` e :math:`\eta=\eta(x, y)`. Podemos reescrever as derivadas que aparecem na :eq:`chap1eq1` em termos destas novas variáveis.



:math:`\phantom{\text{paragr}}` Considerando as novas variáveis, podemos reescrever as derivadas de primeira ordem da seguinte forma:

.. math::
  :label: primeiraderiv

    \begin{aligned}
    u_{i}=U_{\xi}\xi_{i}+U_{\eta}\eta_{i}, \quad i=x,y,
    \end{aligned}
    
onde definimos :math:`u(x, y)=u(x(\xi,\eta),y(\xi,\eta))=U(\xi,\eta)` e vamos escrever uma EDP para determinar :math:`U(\xi,\eta)`. Tomando a derivada da :eq:`primeiraderiv`, segue que:

.. math::
  :label: segundaderiv

    \begin{aligned} u_{ij}=\xi_{ij}U_{\xi}+\eta_{ij}U_{\eta}+\xi_{i}\xi_{j}U_{\xi\xi}+(\xi_{j}\eta_{i}+\xi_{i}\eta_{j})U_{\xi\eta}+\eta_{i}\eta_{j}U_{\eta\eta}, \qquad i, j=x, y.
    \end{aligned}

Substituindo estas derivadas na :eq:`chap1eq2`, temos a seguinte derivada:

.. math::

    \begin{multline} A[\xi_{xx}U_{\xi}+\eta_{xx}U_{\eta}+\xi^{2}_{x}U_{\xi\xi}+2\xi_{x}\eta_{x}U_{\xi\eta}+\eta^{2}_{x}U_{\eta\eta}] \\+B[\xi_{xy}U_{\xi}+\eta_{xy}U_{\eta}+\xi_{x}\xi_{y}U_{\xi\xi}+(\xi_{y}\eta_{x}+\xi_{x}\eta_{y})U_{\xi\eta}+\eta_{x}\eta_{y}U_{\eta\eta}] \\+C[\xi_{yy}U_{\xi}+\eta_{yy}U_{\eta}+\xi^{2}_{y}U_{\xi\xi}+2\xi_{y}\eta_{y}U_{\xi\eta}+\eta^{2}_{y}U_{\eta\eta}]
    \\+D[U_{\xi}\xi_{x}+U_{\eta}\eta_{x}]
    +E[U_{\xi}\xi_{y}+U_{\eta}\eta_{y}]
    +FU
    =G,
    \end{multline}
    
e redistribuindo, segue que:

.. math::

    \begin{multline} A\xi_{xx}U_{\xi}+A\eta_{xx}U_{\eta}+A\xi^{2}_{x}U_{\xi\xi}+2A\xi_{x}\eta_{x}U_{\xi\eta}+A\eta^{2}_{x}U_{\eta\eta} \\B\xi_{xy}U_{\xi}+B\eta_{xy}U_{\eta}+B\xi_{x}\xi_{y}U_{\xi\xi}+B(\xi_{y}\eta_{x}+\xi_{x}\eta_{y})U_{\xi\eta}+B\eta_{x}\eta_{y}U_{\eta\eta}\\+C\xi_{yy}U_{\xi}+C\eta_{yy}U_{\eta}+C\xi^{2}_{y}U_{\xi\xi}+2C\xi_{y}\eta_{y}U_{\xi\eta}+C\eta^{2}_{y}U_{\eta\eta}\\+DU_{\xi}\xi_{x}+DU_{\eta}\eta_{x}+EU_{\xi}\xi_{y}+EU_{\eta}\eta_{y}
    +FU=G,
    \end{multline}

o que nos permite escrever:

.. math::
    \begin{multline*}(A\xi^{2}_{x}+B\xi_{x}\xi_{y}+C\xi^{2}_{y})U_{\xi\xi}\\+[2A\xi_{x}\eta_{x}+B(\xi_{y}\eta_{x}+\xi_{x}\eta_{y})+2C\xi_{y}\eta_{y}]U_{\xi\eta}\\+(A\eta^{2}_{x}+B\eta_{x}\eta_{y}+C\eta^{2}_{y})U_{\eta\eta}\\+(A\xi_{xx}+B\xi_{xy}+C\xi_{yy}+D\xi_{x}+E\xi_{y})U_{\xi}\\+(A\eta_{xx}U_{\eta}+B\eta_{xy}+C\eta_{yy}+D\eta_{x}+E\eta_{y})U_{\eta}
    \\+FU=G.
    \end{multline*}

Vemos que podemos definir novos coeficientes para a equação acima. Assim, temos:

.. math::
    \begin{aligned}A^{\prime}&=A\xi^{2}_{x}+B\xi_{x}\xi_{y}+C\xi^{2}_{y}\\B^{\prime}&=2A\xi_{x}\eta_{x}+B(\xi_{y}\eta_{x}+\xi_{x}\eta_{y})+2C\xi_{y}\eta_{y}\\C^{\prime}&=A\eta^{2}_{x}+B\eta_{x}\eta_{y}+C\eta^{2}_{y}\\D^{\prime}&=A\xi_{xx}+B\xi_{xy}+C\xi_{yy}+D\xi_{x}+E\xi_{y}\\E^{\prime}&=A\eta_{xx}U_{\eta}+B\eta_{xy}+C\eta_{yy}+D\eta_{x}+E\eta_{y}\\F^{\prime}&=FU\\G^{\prime}&=G.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Com estes novos coeficientes, temos a seguinte EDP:

.. math::
  :label: coefsedps2

    \begin{aligned}             A^{\prime}U_{\xi\xi}+B^{\prime}U_{\xi\eta}+C^{\prime}U_{\eta\eta}+D^{\prime}U_{\xi}+E^{\prime}U_{\eta}+F^{\prime}U=G^{\prime}.
    \end{aligned}


Forma Canônica das EDPs
************************


:math:`\phantom{\text{paragr}}` A :eq:`coefsedps2` é matematicamente idêntica à :eq:`chap1eq2`, no entanto, agora temos a liberdade de escolha das funções :math:`\xi` e :math:`\eta` de modo que algumas das derivadas de segunda ordem que aparecem na :eq:`coefsedps2` podem ser eliminadas. Esta simplificação reduz a EDP à uma forma onde apenas uma ou duas derivadas de segunda ordem aparecem e, neste caso, dizemos que a EDP está na forma *canônica*.  A vantagem de reduzir a EDP à alguma forma canônica reside no fato  das formas canônicas terem sido amplamente estudadas ao longo dos anos e, deste modo, várias técnicas para sua solução foram desenvolvidas. Para citar alguns exemplos, a equação de Laplace, cuja forma é :math:`U_{\xi\xi}+U_{\eta\eta}=0`, é uma EDP elíptica obtida da redução à forma canônica da :eq:`coefsedps2`. De forma análoga, a equação do calor :math:`c^{2}U_{\xi\xi}-U_{\eta}=0`, e a equação da onda, :math:`c^{2}U_{\xi\xi}-U_{\eta\eta}=0`, com :math:`c=\text{const.}`, são exemplos de EDPs parabólica e hiperbólica, respectivamente, obtidas da redução da :eq:`coefsedps2` à forma canônica.



:math:`\phantom{\text{paragr}}` Vejamos então como fazemos a redução da :eq:`coefsedps2` à forma canônica e como obtemos as diversas formas de EDPs. O primeiro passo foi a transformação de variáveis que efetuamos e, a partir desta transformação, podemos escolher as funções :math:`\xi` e :math:`\eta`. Temos como objetivo eliminar o maior número possível de derivadas de ordem mais alta. Para isso, vamos determinar :math:`\xi` e :math:`\eta` tal que :math:`A^{\prime}=C^{\prime}=0`. Usando a definição que construímos para :math:`A^{\prime}` e :math:`C^{\prime}`, temos:

.. math::
   :label: eqAprime

    \begin{aligned}
    A^{\prime}=A\xi^{2}_{x}+B\xi_{x}\xi_{y}+C\xi^{2}_{y}=0,
    \end{aligned}

e,

.. math::
   :label: eqCprime

   \begin{aligned}
    C^{\prime}=A\eta^{2}_{x}+B\eta_{x}\eta_{y}+C\eta^{2}_{y}=0.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Podemos reescrever as equações :eq:`eqAprime` e :eq:`eqCprime` da seguinte forma:

.. math::
   :label: eqBaskahraA1

    \begin{aligned}
    A\left[\dfrac{\xi_{x}}{\xi_{y}}\right]^{2}+B\dfrac{\xi_{x}}{\xi_{y}}+C=0,
    \end{aligned}

e,

.. math::
   :label: eqBaskahraA

   \begin{aligned}
    A\left[\dfrac{\eta_{x}}{\eta_{y}}\right]^{2}+B\dfrac{\eta_{x}}{\eta_{y}}+C=0.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Ambas as equações apresentam a mesma solução, :math:`(-B\pm\sqrt{B^{2}-4AC})/2A`, porém, para obtemos diferentes soluções para :math:`\eta_{x}/\eta_{y}` e :math:`\xi_{x}/\xi_{y}`, vamos usar raízes diferentes para cada uma das curvas, assim, escrevemos:

.. math::
  :label: xisolucoes

    \begin{aligned}
    \dfrac{\xi_{x}}{\xi_{y}}=\dfrac{-B+\sqrt{B^{2}-4AC}}{2A}
    \end{aligned}

e,

.. math::
  :label: etasolucoes

    \begin{aligned}
    \dfrac{\eta_{x}}{\eta_{y}}=\dfrac{-B-\sqrt{B^{2}-4AC}}{2A}.
    \end{aligned}

:math:`\phantom{\text{paragr}}` A partir da :eq:`xisolucoes`  e :eq:`etasolucoes`, podemos entender a condição colocada no terceiro parágrafo, onde classificamos as EDPs de acordo com o discriminante :math:`B^{2}-4AC`. Quando :math:`B^{2}-4AC>0`, temos duas soluções reais e, com isso, tanto :math:`A^{\prime}` quanto :math:`C^{\prime}` podem ser iguais a zero. Neste caso, somente  :math:`B^{\prime}\neq0` e temos uma equação hiperbólica. Outra possibilidade em que :math:`B^{2}-4AC>0`, é fazendo :math:`B=0` e :math:`C=-A`. Neste caso as derivadas de segunda ordem tomam a forma :math:`U_{\xi\xi}-U_{\eta\eta}`. Esta seria também uma forma canônica hiperbólica. Quando :math:`B^{2}-4AC=0`, temos apenas uma solução. Assim, somente um dos coeficientes pode ser zerado, ou seja, ou :math:`A^{\prime}` ou :math:`C^{\prime}`. Além disso, como o coeficiente :math:`B^{\prime}` é proporcional ao discriminante (mostraremos isso logo abaixo), este também se torna igual a zero neste caso. Como resultado, temos uma equação parabólica onde contamos com apenas um termo contendo derivada de segunda ordem. Há ainda um terceiro caso onde não temos nenhuma solução real, significando que ambos :math:`A^{\prime}` e :math:`C^{\prime}` são diferentes de zero. Neste caso, é possível escolher :math:`\xi` e :math:`\eta` de maneira que o coeficiente :math:`B^{\prime}=0` e temos o caso elíptico.


:math:`\phantom{\text{paragr}}` A seguir, vamos considerar cada um dos três casos separadamente e a forma da EDP após a tranformação canônica.

Caso Hiperbólico
----------------

:math:`\phantom{\text{paragr}}` O caso hiperbólico é definido pela condição :math:`B^{2}-4AC>0` e, como pontuamos acima, temos duas soluções reais para :eq:`xisolucoes` e :eq:`etasolucoes`. Assim, é possível escolher :math:`\xi` e :math:`\eta` de maneira que :math:`A^{\prime}=C^{\prime}=0`. Neste caso, a EDP dada pela :eq:`coefsedps2` fica reduzida à seguinte forma:

.. math::
  :label: hyperfirstform

    \begin{aligned}
    U_{\xi\eta}=\Phi(\xi, \eta, U, U_{\xi},U_{\eta})
    \end{aligned}

onde definimos :math:`\Phi(\xi,\eta,U,U_{\xi},U_{\eta})` contendo os demais termos da :eq:`coefsedps2` que não são derivadas de segunda ordem.

:math:`\phantom{\text{paragr}}` A determinação de :math:`\eta` e :math:`\xi` é dada a partir das :eq:`xisolucoes` e :eq:`etasolucoes`. Podemos escrever essas equações na forma mais compacta abaixo:

.. math::
  :label: eq1xietadeterm

    \begin{aligned}
    \mu_{1}=\dfrac{\xi_{x}}{\xi_{y}},\quad \mu_{2}=\dfrac{\eta_{x}}{\eta_{y}}
    \end{aligned}

onde chamamos as raízes de :math:`\mu_{1}` e :math:`\mu_{2}`. Note que no caso mais geral que em os coeficientes são funções e não apenas constantes como estamos considerando aqui, :math:`\mu_{1}` e :math:`\mu_{2}` são funções de :math:`x` e :math:`y`, desde que estão relacionadas à EDP original por meio dos coeficientes :math:`A`, :math:`B` e :math:`C`.

:math:`\phantom{\text{paragr}}` Vamos assumir que :math:`\xi` e :math:`\eta` são constantes. Neste caso, podemos escrever:

.. math::

    \begin{aligned}
    d\xi=\xi_{x}dx+\xi_{y}dy=0
    \\
    d\eta=\eta_{x}dx+\eta_{y}dy=0
    \end{aligned}

e, a partir destas duas equações, podemos escrever:

.. math::
  :label: condicaoxieta

    \begin{aligned}
    \dfrac{\xi_{x}}{\xi_{y}}=-\dfrac{dy}{dx}=\mu_{1},\quad \dfrac{\eta_{x}}{\eta_{y}}=-\dfrac{dy}{dx}=\mu_{2},
    \end{aligned}

o que nos permite determinar a razão entre as derivadas das funções em termos da relação entre :math:`y` e :math:`x`.

:math:`\phantom{\text{paragr}}` As equações dadas por :eq:`condicaoxieta` são chamadas de equações características da PDE. A partir delas podemos determinar as curvas :math:`y=y(x)` sobre as quais :math:`\xi` e :math:`\eta` são constantes. No caso particular em que todos os coeficientes da PDE são constantes, temos duas famílias de retas no plano-:math:`xy`, de fato, integrando as equações acima, obtemos:

.. math::

    \begin{aligned}
    y(x)=-\mu_{1}x+K_{1},\qquad y(x)=-\mu_{2}x+K_{2}
    \end{aligned}

ou ainda,

.. math::

    \begin{aligned}
    y+\mu_{1}x=K_{1},\qquad y+\mu_{2}x=K_{2}
    \end{aligned}


onde :math:`K_{1}` e :math:`K_{2}` são constantes, resultado da integração. Como :math:`\xi` e :math:`\eta` devem ser constantes, então podemos escrever:

.. math::

    \begin{aligned}
    \xi(x,y)=y+\mu_{1}x,\qquad \eta(x,y)=y+\mu_{2}x.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Agora resta confirmar que estes dois pares de retas realmente reduzem a EDP inicial à forma canônica. Para isso, notamos que :math:`\xi_{x}=\mu_{1}`, :math:`\xi_{y}=1`, :math:`\eta_{x}=\mu_{2}` e :math:`\eta_{y}=1`. Substituindo estes valores das derivadas nas equações dos coeficientes da EDP dada pela :eq:`coefsedps2`, segue que:

.. math::

    \begin{aligned}
    A^{\prime}&=A\mu^{2}_{1}+B\mu_{1}+C=0, \qquad \mu_{1}~\text{é raíz.}\\
    B^{\prime}&=-\dfrac{(B^{2}-4AC)}{A},\\
    C^{\prime}&=A\mu^{2}_{2}+B\mu_{2}+C=0, \qquad \mu_{2}~\text{é raíz.}\\
    D^{\prime}&=D\mu_{1}+E,\\
    E^{\prime}&=D\mu_{2}+E,\\
    F^{\prime}&=F,\\
    G^{\prime}&=G,\\
    \end{aligned}

e vemos que a forma final da EDP é igual à forma dada pela :eq:`hyperfirstform`. É importante notar que embora tenhamos realizado a demonstração para o caso de coeficientes constantes, essa afirmação é válida para o caso mais geral em que a EDP é hiperbólica apenas localmente, ou seja, para determinados valores de :math:`x` e :math:`y`. De fato, assumindo que existam raízes :math:`\mu_{1}=\mu_{1}(x,y)` e :math:`\mu_{2}=\mu_{2}(x,y)` que resolvem os coeficientes :math:`A^{\prime}` e :math:`C^{\prime}`, então o coeficiente :math:`B^{\prime}` pode ser escrito na forma:

.. math::

    \begin{aligned}
    B^{\prime}=\xi_{y}\eta_{y}\left[\dfrac{\xi_{x}}{\xi_{x}}\dfrac{\eta_{y}}{\eta_{y}}+B\left(\dfrac{\xi_{x}}{\xi_{y}}+\dfrac{\eta_{x}}{\eta_{y}}\right)+2C\right],
    \end{aligned}

onde colocamos o produto de derivadas :math:`\xi_{y}\eta_{y}` em evidência. Agora notamos que as razões das derivadas são iguais às raízes conforme :eq:`eq1xietadeterm`. Assim, como a soma das raízes deve ser igual a :math:`-B/A` e o produto das raízes deve ser igual a :math:`C/A`, então podemos escrever :math:`B^{\prime}` na forma:

.. math::
   :label: rotuloBprime

    \begin{aligned}
    B^{\prime}=\xi_{y}\eta_{y}\left[\dfrac{C}{A}+B\left(-\dfrac{B}{A}\right)+2C\right]=-\dfrac{\xi_{y}\eta_{y}}{A}(B^{2}-4AC).
    \end{aligned}

:math:`\phantom{\text{paragr}}` Portanto, vemos que o coeficiente :math:`B^{\prime}` apresenta foma similar à encontrada para o caso de coeficientes constantes, porém, aparecm explicitamente as derivadas de :math:`\xi` e :math:`\eta` explicitamente.


Segunda forma canônica para o caso hiperbólico
++++++++++++++++++++++++++++++++++++++++++++++


Foi mencionado uma segunda forma canônica para o caso hiperbólico. Esta forma pode mais facilmente ser obtida por meio de uma troca de variáveis na :eq:`hyperfirstform`. Assim, definimos:

.. math::
  :label: trocavariavel

    \begin{aligned}
    \alpha=\xi+\eta,
    \\
    \beta=\xi-\eta.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Usando a regra da cadeia, podemos escrever:

.. math::
  :label: novasderivadas

    \begin{aligned}
    U_{\xi}=\alpha_{\xi}V_{\alpha}+\beta_{\xi}V_{\beta},
    \\
    U_{\eta}=\alpha_{\eta}V_{\alpha}+\beta_{\eta}V_{\beta},
    \end{aligned}

onde definimos, :math:`U(\xi(\alpha,\beta),\eta(\alpha,\beta))=V(\alpha,\beta)`. Agora que sabemos a relação simples entre as funções :math:`\alpha` e :math:`\beta` com as funções :math:`\xi` e :math:`\eta`, então, usando a :eq:`trocavariavel`, podemos escrever :math:`\alpha_{\xi}=1`, :math:`\beta_{\xi}=1`, :math:`\alpha_{\eta}=1` e :math:`\beta_{\eta}=-1`. Usando essas relações na equação anterior, segue que:

.. math::
  :label: novasderv2

    \begin{aligned}
    U_{\xi}=V_{\alpha}+V_{\beta},
    \\
    U_{\eta}=V_{\alpha}-V_{\beta},
    \end{aligned}

e, tomando a derivada em relação à :math:`\xi` na segunda expressão de :eq:`novasderv2`, segue que:

.. math::

    \begin{aligned}
    U_{\xi\eta}&=(\partial_{\alpha}+\partial_{\beta})(V_{\alpha}-V_{\beta})\\
    &=V_{\alpha\alpha}-V_{\alpha\beta}+V_{\beta\alpha}-V_{\beta\beta}=V_{\alpha\alpha}-V_{\beta\beta},
    \end{aligned}

onde usamos o fato de que :math:`V_{\alpha\beta}=V_{\beta\alpha}` e, deste modo, os termos mistos são nulos. Assim, a nova EDP nas variáveis :math:`\alpha` e :math:`\beta`, pode ser escrita na forma:

.. math::
  :label: novaEDPalphabeta

    \begin{aligned}
    V_{\alpha\alpha}-V_{\beta\beta}=\Psi(\alpha,\beta, V,V_{\alpha},V_{\beta}),
    \end{aligned}

onde a função :math:`\Psi(\alpha,\beta, V,V_{\alpha},V_{\beta})` contém os demais termos, sem derivadas de segunda ordem.


Exemplo
+++++++

**1.** *Reduza a EDP abaixo à forma canônica.*

.. math::

    \begin{aligned}
    u_{xx}+u_{xy}-2u_{yy}=0
    \end{aligned}

.. centered:: Solução


:math:`\phantom{\text{paragr}}` Comparando a equação acima com a EDP dada pela :eq:`chap1eq2`, temos que :math:`A=1`, :math:`B=1`, :math:`C=-2` e :math:`D=E=F=G=0`.

:math:`\phantom{\text{paragr}}` Com estas informações, podemos determinar as razões :math:`\xi_{x}/\xi_{y}` e :math:`\eta_{x}/\eta_{y}`. Com efeito, podemos escrever:


.. math::

    \begin{aligned}
    \dfrac{\xi_{x}}{\xi_{y}}&=\dfrac{-B+\sqrt{B^{2}-4AC}}{2A}=\dfrac{-1+\sqrt{(-1)^{2}-4\cdot 1\cdot(-2)}}{2\cdot1}=1,
    \\
    \dfrac{\eta_{x}}{\eta_{y}}&=\dfrac{-B-\sqrt{B^{2}-4AC}}{2A}=\dfrac{-1+\sqrt{(-1)^{2}-4\cdot 1\cdot(-2)}}{2\cdot1}=-2.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Com estas duas expressões temos duas equações diferenciais ordinárias para resolver para :math:`y(x)` que nos ajudarão a determinar :math:`\xi` e :math:`\eta`. De fato, temos que:

.. math::

    \begin{aligned}
    \dfrac{dy}{dx}=-1\qquad \Rightarrow \qquad y(x)=-x+K_{1},\qquad \Rightarrow \qquad y+x=K_{1},
    \end{aligned}

e também temos que:

.. math::

    \begin{aligned}
    \dfrac{dy}{dx}=2\qquad\Rightarrow\qquad y(x)=2x+K_{2}~\qquad\Rightarrow\qquad y-2x=K_{2},
    \end{aligned}

onde :math:`K_{1}` e :math:`K_{2}` são constantes. Como partimos do pressuposto que :math:`\xi` e :math:`\eta` são constantes, então podemos escrever:

.. math::

    \begin{aligned}
    \xi(x,y)=y+x,\quad\text{e}\qquad\eta(x,y)=y-2x.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Com estas duas equações, podemos determinar as derivadas, assim temos que :math:`\xi_{x}=1`, :math:`\xi_{y}=1`, :math:`\eta_{x}=-2` e :math:`\eta_{y}=1`.

:math:`\phantom{\text{paragr}}` Usando as derivadas podemos determinar os coeficientes da :eq:`coefsedps2`, assim, temos:

.. math::

    \begin{aligned}
    A^{\prime}=0,~B^{\prime}=-9,~C^{\prime}=D^{\prime}=E^{\prime}=F^{\prime}=G^{\prime}=0.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Usando a forma da :eq:`coefsedps2`, podemos escrever a equação do enunciado assume a seguinte forma canônica:

.. math::

    \begin{aligned}
   -9U_{\xi\eta}=0.
    \end{aligned}

ou seja,

.. math::

    \begin{aligned}
    U_{\xi\eta}=0.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Esta equação pode ser facilmente resolvida notando que se a derivada em relação :math:`\xi` é igual a zero, então :math:`U_{\eta}=g(\eta)`. No entanto, para que a derivada seja uma função de :math:`\eta` somente, então devemos ter que o próprio :math:`U(\eta,\xi)=f_{1}(\eta)+f_{2}(\xi)`. Chegamos ao mesmo resultado para o caso em que a ordem das derivadas é trocada,ou seja, para :math:`U_{\eta\xi}`.


:math:`\phantom{\text{paragr}}` Com isso, podemos escrever:

.. math::

    \begin{aligned}
    U(\xi,\eta)=f_{1}(\eta)+f_{2}(\xi),
    \end{aligned}

ou, em termos das variáveis :math:`x` e :math:`y`, temos a seguinte solução geral:

.. math::

    \begin{aligned}
    u(x,y)=g_{1}(y-2x)+g_{2}(x+y).
    \end{aligned}


Caso Parabólico
---------------

:math:`\phantom{\text{paragr}}` O caso parabólico é definido pela condição :math:`B^{2}-4AC=0` e, com isso, temos a possibilidade de eliminar ou o termo envolvendo :math:`A^{\prime}` ou o termo envolvendo :math:`C^{\prime}`. Além disso, a :eq:`rotuloBprime` permanece válida no caso parabólico, assim :math:`B^{\prime}=0`, e ficamos apenas com uma única derivada de segunda ordem.  Escolhendo o termo :math:`A^{\prime}` para ser eliminado, a forma geral da equação parabólica é dada por:

.. math::
  :label: formageralparabolica

    \begin{aligned}
    C^{\prime}U_{\eta\eta}+D^{\prime}U_{\xi}+E^{\prime}U_{\eta}+F^{\prime}U=G.
    \end{aligned}

:math:`\phantom{\text{paragr}}` Alternativamente, poderíamos ter eliminado o termo envolvendo :math:`C^{\prime}`. Neste caso, a EDP teria a forma dada abaixo:

.. math::
  :label: formageralparabolica2

    \begin{aligned}
    A^{\prime}U_{\xi\xi}+D^{\prime}U_{\xi}+E^{\prime}U_{\eta}+F^{\prime}U=G.
    \end{aligned}


Exemplo
+++++++


**2.** *Reduza a EDP abaixo à forma canônica.*

.. math::

    \begin{aligned}
    u_{xx}-2u_{xy}+u_{yy}=0
    \end{aligned}

.. centered:: Solução

:math:`\phantom{\text{paragr}}` Vamos determinar a equação característica para as funções :math:`\xi` e :math:`\eta`. Para isso, seguimos a prescrição definida acima, i.e.,

.. math::

    \begin{aligned}           \dfrac{\xi_{x}}{\xi_{y}}=\dfrac{-B+\sqrt{B^{2}-4AC}}{2A}=\dfrac{2+\sqrt{4-4.1.1}}{2}=1.\end{aligned}





:math:`\phantom{\text{pargraph}}` Vemos que como :math:`B^{2}-4AC=0`, podemos eliminar apenas :math:`A^{\prime}` ou :math:`C^{\prime}` mas não as duas constantes ao mesmo tempo como no caso hiperbólico. Assim prosseguimos assumindo que :math:`A^{\prime}=0` de modo que :math:`\xi` é definida pela equação acima enquanto :math:`\eta` permanece arbitrário. Segue então que:

.. math::

    \begin{aligned}
    \dfrac{\xi_{x}}{\xi_{y}}=-\dfrac{dy}{dx}=1\qquad \dfrac{dy}{dx}=-1,
    \end{aligned}

e integrando, obtemos a seguinte equação:

.. math::

    \begin{aligned}
    y=-x+C\qquad C=y+x,
    \end{aligned}

onde :math:`C` é uma constante e, como todo o desenvolvimento acima foi construído baseando-se na premissa de que :math:`\xi=\text{const.}`, segue que:

.. math::

    \begin{aligned}
    \xi(x,y)=x+y.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` Temos ainda que:

.. math::

    \begin{aligned}
    \xi_{xx}=\xi_{y}=1,\qquad \xi_{xx}=\xi_{xy}=\xi_{yy}=0.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` Para zerar o coeficiente :math:`B^{\prime}` da EDP, escolhemos :math:`\eta(x)=x`, assim, :math:`\eta_{x}=1` e :math:`\eta_{y}=0`. Conforme mencionamos acima, a função :math:`\eta` ficou arbitrária porque não temos uma equação definindo o coeficiente :math:`C^{\prime}` a partir da curva :math:`y(x)` como obtivemos para a função :math:`\xi(x,y)`. Além disso, :math:`\eta_{xx}=\eta_{xy}=\eta_{yy}=0`. Com estes dados temos que: :math:`D^{\prime}=E^{\prime}=0` e, da mesma forma, :math:`F^{\prime}=G^{\prime}=0`.


:math:`\phantom{\text{pargraph}}` O coeficiente :math:`C^{\prime}` é dado por:

.. math::

    \begin{aligned}
    C^{\prime}=A\eta^{2}_{x}+B\eta_{x}\eta_{y}+C\eta_{yy}
    \end{aligned}

e, após a substituição das derivadas, obtemos :math:`C^{\prime}=1`. Com estas considerações, podemos escrever a EDP transformada nas novas variáveis :math:`\xi` e :math:`\eta`, assim, temos:

.. math::

    \begin{aligned}
    U_{\eta\eta}=0.
    \end{aligned}


:math:`\phantom{\text{pargraph}}` A solução da equação acima é dada por:

.. math::

    \begin{aligned}
    U(\xi,\eta)=\eta F(\xi)+G(\eta),
    \end{aligned}

o que pode ser facilmente demonstrado integrando a equação acima duas vezes em relação à variável :math:`\eta`.

:math:`\phantom{\text{pargraph}}` Retornando às variáveis antigas :math:`x` e :math:`y`, vamos obter:

.. math::

    \begin{aligned}
    u(x,y)=x f(x+y)+g(x+y).
    \end{aligned}

Caso Elíptico
-------------

:math:`\phantom{\text{pargraph}}` Vamos considerar o último caso definido pela condição :math:`B^{2}-4AC<0`, que chamamos de caso elíptico. Neste caso não temos raízes reais na equação característica mas podemos reescrever a EDP em uma forma mais simples conforme ilustrado no exemplo abaixo.

Exemplo
+++++++

**3.** *Reduza a EDP abaixo à forma canônica.*

.. math::

    \begin{aligned}
    4u_{xx}-4u_{xy}+5u_{yy}=0.
    \end{aligned}

.. centered:: Solução

:math:`\phantom{\text{pargraph}}` O procedimento aqui é o mesmo adotado nos exemplos anteriores. Assim, identificando na equação dada no enunciado :math:`A=4, B=-4, C=5, D=E=F=G=0`. Com isso, temos que:

.. math::

    \begin{aligned}
    \dfrac{\xi_{x}}{\xi_{y}}=\dfrac{1}{2}+i,\qquad
    \dfrac{\eta_{x}}{\eta_{y}}=\dfrac{1}{2}-i.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` O procedimento que se segue é similar aos exemplos anteriores e deixamos para o leitor demonstrar que:

.. math::

    \begin{aligned}
    \xi(x,y)&=y+\left(\dfrac{1}{2}+i\right)x,
    \\
    \eta(x,y)&=y+\left(\dfrac{1}{2}-i\right)x,
    \end{aligned}

o que nos permite escrever :math:`\xi_{x}=1/2+i`, :math:`\eta_{x}=1/2-i`, :math:`\xi_{y}=\eta_{y}=1`. Desde que as primeiras derivadas são constantes, todas as derivadas de segunda ordem são nulas, assim, com estes dados podemos determinar os coeficientes da EDP nas variáveis :math:`\xi` e :math:`\eta`  (c.f., :eq:`coefsedps2`).

:math:`\phantom{\text{pargraph}}` Deixando para o leitor demonstrar que :math:`A^{\prime}=C^{\prime}=0`, :math:`B^{\prime}=16`, a nova EDP tem a forma:

.. math::

    \begin{aligned}
    U_{\xi\eta}=0.
    \end{aligned}


:math:`\phantom{\text{pargraph}}` As funções :math:`\eta` e :math:`\xi` são complexas devido ao fato de que :math:`B^{2}-4AC<0`. Podemos trabalhar com funçõess reais definindo um novo par de variáveis :math:`\alpha` e :math:`\beta` da seguinte forma:

.. math::

    \begin{aligned}
    \xi(\alpha,\beta)&=\alpha+i\beta,
    \\
    \eta(\alpha,\beta)&=\alpha-i\beta.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` Com estas definições, obtemos :math:`\alpha=y+x/2` e :math:`\beta=x`. Além disso, usando a regra da cadeia, podemos escrever a EDP :math:`U_{\xi\eta}=0` em termos das novas variáveis, o que nos leva à seguinte EDP:

.. math::

    \begin{aligned}
    U_{\alpha\alpha}+U_{\beta\beta}=0.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` Podemos resolver a EDP acima fazendo :math:`U_{\alpha\alpha}=-U_{\beta\beta}` e esta igualdade pode ser satisfeita sem ambos os lados foram iguais a constantes. Assim, definindo :math:`\lambda` uma constante em relação às variáveis :math:`\alpha` e :math:`\beta`, temos duas EDOs: :math:`U_{\alpha\alpha}-\lambda=0` e :math:`U_{\beta\beta}+\lambda=0`. A solução destas duas EDOs é bem simples, assim, a solução geral da EDP pode ser colocada na forma:

.. math::

    \begin{aligned}
    U(\alpha,\beta)=\dfrac{\lambda}{2}(\alpha^{2}-\beta^{2})+C,
    \end{aligned}

onde :math:`C` é uma constante.

:math:`\phantom{\text{pargraph}}` Com a solução acima, podemos voltar às variáveis originais, assim, a solução geral para a EDP se torna:

.. math::

    \begin{aligned}
    u(x,y)=\dfrac{\lambda}{2}\left[\left(y+\dfrac{x}{2}\right)-x^{2}\right]+C^{\prime}
    \end{aligned}

onde :math:`C^{\prime}` é uma constante.



.. admonition:: `Referências Recomendadas`

     [1] E. Kreyszig. Advanced Engineering Mathematics. John Wiley, 2006. ISBN 9780471728979. URL: https://books.google.com.br/books?id=3t7DQgAACAAJ.

     [2] P. DuChateau and D. Zachmann. Schaum's Outline of Partial Differential Equations. Schaum's Outline Series. McGraw-Hill Education, 2011. ISBN 9780071756181. URL: https://books.google.com.br/books?id=MDjEbwAACAAJ.

     [3] G.B. Arfken and H.J. Weber. Mathematical Methods For Physicists. Elsevier Science, 2005. ISBN 9780080470696. URL: https://books.google.com.br/books?id=tNtijk2iBSMC.

     [4] K. D. Machado. Equações Diferenciais Aplicadas, vol. 2. Toda Palavra Editora, 2018. ISBN 9788562450518.
