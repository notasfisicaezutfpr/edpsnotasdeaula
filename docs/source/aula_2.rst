A equação da onda
=================

*Referência: E. Kreyszig, Advanced Engineering Mathematics, 9th ed, Ch. 12*

:math:`\phantom{\text{pargraph}}` No capítulo anterior, discutimos a forma canônica das EDPs e, dentro daquele contexto, mostramos que as EDPs podem ser classificadas como hiperbólicas, elípticas e parabólicas. Há vários exemplos de aplicação onde cada uma destas equações aparecem e, neste capítulo, vamos discutir a *equação da onda*. Vamos considerar o caso particular da equação da onda para o caso unidimensional, usada para modelar uma corda vibrante.

:math:`\phantom{\text{pargraph}}` A equação da onda unidimensional (EQO) permite determinar a amplitude da vibração da corda, denotada aqui por :math:`u(x,t)`, em função da posição horizontal :math:`x` e do instante de tempo :math:`t`. A EQO é definida pela :eq:`chap2eq1`:

.. math::
  :label: chap2eq1

    \begin{aligned}
   \dfrac{\partial^{2} u}{\partial x^{2}} - \dfrac{1}{c^{2}}\dfrac{\partial^{2} u}{\partial t^{2}}=0.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` A :eq:`chap2eq1` é um exemplo de uma equação hiperbólica e, desta forma, podemos reduzí-la à forma canônica definindo novas variáveis :math:`\xi` e :math:`\eta`. É possível reescrever a EQO na forma :math:`U_{\xi\eta}=0` de modo que a sua solução geral é dada por :math:`U(\xi,\eta)=f(\xi)+g(\eta)` onde :math:`(\xi,\eta)=x\pm ct`. Deixamos a demonstração destas relações por conta do leitor.

:math:`\phantom{\text{pargraph}}` O objetivo é aplicar a EQO para o caso de uma corda vibrante e, para isso, precisamos definir as condições de contorno e as condições iniciais. Vamos assumir que a corda em questão tem comprimento :math:`L` e está presa em suas duas extremdidades.

.. admonition:: Condições de Contorno & Condições Iniciais

    Condições de Contorno
                .. math::
                  :label: condcontornoEQO

                    \begin{aligned}
                   u(0,t)&=0\\
                   u(L,t)&=0.
                    \end{aligned}

    Condições Iniciais
                .. math::
                  :label: condinicialEQO

                    \begin{aligned}
                   u(x,t)=f(x)\\
                   \left.\dfrac{\partial u(x,t)}{\partial t}\right]_{t=0}=g(x).
                    \end{aligned}

:math:`\phantom{\text{pargraph}}` A condição de contorno diz respeito à posição da corda nas extremidades cuja amplitude deve ser zero, desde que estamos assumindo que a corda está presa em suas extremidades. Assim, a condição de contorno diz respeito à dependência da amplitude com a posição :math:`x`. As condições iniciais determinam a posição e a velocidade da corda como um todo em um dado instante inicial, o qual tomamos aqui como :math:`t=0`. Definimos duas funções :math:`f(x)`, para descrever a posição da corda no instante inicial e :math:`g(x)` para descrever a velocidade inicial da corda.

Separação de Variáveis
++++++++++++++++++++++

:math:`\phantom{\text{pargraph}}` Vamos resolver a EQO por meio da técnica de separação de variáveis. Para isso, vamos partir da premissa de que a ampltitude da corda é descrita por :math:`u(x,t)=X(x)T(t)`, ou seja, dada pelo produto de duas funções :math:`X` e :math:`T` de uma variável. Substituindo :math:`u(x,t)` na EQO dada pela :eq:`chap2eq1`, obtemos a seguinte expressão:

.. math::

    \begin{aligned}
    T(t)\dfrac{d^{2}X(x)}{dx^{2}} - \dfrac{1}{c^{2}}X(x)\dfrac{d^{2}T(t)}{dt^{2}}=0,
    \end{aligned}

onde trocamos as variáveis parciais por derivadas totais porque temos funções de apenas uma única variável. Divindindo a equação acima pelo produto :math:`XT` obtemos:

.. math::

    \begin{aligned}
    \dfrac{1}{X(x)}\dfrac{d^{2}X(x)}{dx^{2}} = \dfrac{1}{c^{2}T(t)}\dfrac{d^{2}T(t)}{dt^{2}},
    \end{aligned}

onde passamos a parte dependente do tempo para o segundo membro. A igualdade acima pode somente ser satisfeita se ambas as equações diferenciais foram iguais  a uma constante. Assim, definindo a constante como :math:`\lambda`, obtemos duas equações diferenciais ordinárias:

.. math::
  :label: eqXx

    \begin{aligned}
    \dfrac{d^{2}X}{dx^{2}} -\lambda X=0
    \end{aligned}

para :math:`X(x)` e,

.. math::
  :label: eqTt

    \begin{aligned}
    \dfrac{d^{2}T}{dt^{2}} -c^{2}\lambda T=0,
    \end{aligned}

para a função :math:`T(t)`.

:math:`\phantom{\text{pargraph}}` As soluções das :eq:`eqXx`  e :eq:`eqTt` são obtidas por meio de métodos estudados em um curso de EDOs. Vamos considerar em detalhes a solução para :math:`X(x)`, para a solução para :math:`T(t)` uma análise similar pode ser realizada. Desde que :math:`\lambda` é uma constante, em princípio temos três possibilidades de solução:

.. math::
  :label: solgeralXposs

    \begin{aligned}
    X(x)&=Ae^{\sqrt{\lambda}x}+Be^{\sqrt{\lambda}x},\qquad \lambda>0,\\
    X(x)&=Ax+B,\qquad \lambda=0,\\
    X(x)&=A\cos(\sqrt{|\lambda|}x)+B\sin(\sqrt{|\lambda|}x),\qquad \lambda<0.
    \end{aligned}

Aplicando as condições de contorno e iniciais
+++++++++++++++++++++++++++++++++++++++++++++

:math:`\phantom{\text{pargraph}}` As três equação acima satisfazem a EQO, porém, além da equação em si, as soluções precisam satisfazer as condições de contorno. Neste sentido, é possível verificar (deixamos para o leitor como exercício), que apenas a solução para :math:`\lambda<0` satisfazem as condições de contorno. Assim, para facilitar a notação, definimos :math:`k=\sqrt{|\lambda|}` de maneira que a solução geral pode ser escrita na forma:

.. math::
  :label: solgeralX

    \begin{aligned}
   X(x)=A\cos kx +B\sin kx.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` Aplicando a primeira condição de contorno, :math:`u(0,t)=X(0)=0`, na :eq:`solgeralX`, obtemos:

.. math::

    \begin{aligned}
    X(0)=A\cos(k\cdot 0)+B\sin(k\cdot 0)=A\qquad\therefore\qquad A=0,
    \end{aligned}

desde que :math:`X(0)=0`. Assim, a :eq:`solgeralX` fica reduzida à seguinte forma:

.. math::
   :label: condcontorno1

    \begin{aligned}
    X(x)=B\sin kx.
    \end{aligned}

:math:`\phantom{\text{pargraph}}` Aplicando a segunda condição de contorno na :eq:`condcontorno1`, segue que:

.. math::

    \begin{aligned}
    X(L)=B\sin kL=0,
    \end{aligned}

desde que :math:`u(L,t)=X(L)=0`. Como não estamos interessados na solução trivial :math:`B=0`, então para que a igualdade acima seja satisfeita, devemos ter:

.. math::
  :label: kL

    \begin{aligned}
   kL=n\pi,\qquad n= 0, 1, 2, 3,\cdots
    \end{aligned}

e, deste modo os valores de :math:`k` que satisfaz a segunda condição de contorno devem ser discretos, assim, a solução geral dada pela :eq:`condcontorno1` se torna:

.. math::
  :label: solparticularX

    \begin{aligned}
   X(x)=B\sin\left(\dfrac{n\pi x}{L}\right).
    \end{aligned}

:math:`\phantom{\text{pargraph}}` A solução para :math:`T(t)` segue as mesmas linhas da solução para :math:`X(x)`, e obtemos:

.. math::
  :label: solgeralT

    \begin{aligned}
   T(t)=C\cos\left(\dfrac{n\pi ct}{L}\right)+D\sin\left(\dfrac{n\pi ct}{L}\right),
    \end{aligned}

onde já estamos utilizando o valor de :math:`k` que deduzimos da solução para :math:`X(x)`, a diferença apenas na velocidade da onda :math:`c` que aparece multiplicando o instante de tempo :math:`t`.

:math:`\phantom{\text{pargraph}}` As constantes :math:`C` e :math:`D` são determinadas pelas condições iniciais destacadas acima. No entanto, o procedimento para obtê-las é diferente do que fizemos para a condição de contorno. Primeiramente, vamos compor a solução geral :math:`u(x, t)=X(x)T(t)`, assim, segue que:

.. math::
  :label: solgeralcompleta1

    \begin{aligned}
   u_{n}(x,t)=\left[C\cos\left(\dfrac{n\pi ct}{L}\right)+D\sin\left(\dfrac{n\pi ct}{L}\right)\right]B\sin\left(\dfrac{n\pi x}{L}\right)
    \end{aligned}

:math:`\phantom{\text{pargraph}}` Podemos reescrever a equação acima definindo novas constantes absorvendo a constante :math:`B` dentro de :math:`C` e :math:`D`. Além disso, devemos indexar as soluções com o índice :math:`n`, desde que temos uma solução geral diferente para cada :math:`n`, assim, também as novas constantes vão ser indexadas por este mesmo índice. Assim, escrevemos:

.. math::
  :label: solgeraluxt

    \begin{aligned}
    u_{n}(x,t)=\left[C_{n}\cos\left(\dfrac{n\pi ct}{L}\right)+D_{n}\sin\left(\dfrac{n\pi ct}{L}\right)\right]\sin\left(\dfrac{n\pi x}{L}\right).
    \end{aligned}

:math:`\phantom{\text{paragr}}` A solução geral será dada pela combinação linear das soluções para todos os inteiros :math:`n`, assim, escrevemos:


.. math::
  :label: solgeraluxt2

    \begin{aligned}
  u(x,t)=\sum_{n=0}^{\infty}\left[C_{n}\cos\left(\dfrac{n\pi ct}{L}\right)+D_{n}\sin\left(\dfrac{n\pi ct}{L}\right)\right]\sin\left(\dfrac{n\pi x}{L}\right).
    \end{aligned}

:math:`\phantom{\text{paragraph}}` A velocidade da corda na direção perpendicular ao seu comprimento pode ser determinada calculando-se a derivada de :math:`u(x,t)` em relação ao tempo. Assim, segue que:

.. math::
  :label: solgeralvelocidade

    \begin{aligned}
  \dfrac{\partial u(x,t)}{\partial t}=\sum_{n=0}^{\infty}\left[-C_{n}\dfrac{n\pi c}{L}\sin\left(\dfrac{n\pi ct}{L}\right)+\dfrac{n\pi c}{L}D_{n}\cos\left(\dfrac{n\pi ct}{L}\right)\right]\sin\left(\dfrac{n\pi x}{L}\right).
    \end{aligned}


:math:`\phantom{\text{paragraph}}` Com as expressões dadas pelas :eq:`solgeralvelocidade` e :eq:`solgeraluxt2`, podemos aplicar as condições iniciais dadas no quadro em destaque acima. Assim, fazendo :math:`t=0` nas equações :eq:`solgeralvelocidade` e :eq:`solgeraluxt2` segue que:


.. math::
   :label: ux0condinit1

    \begin{aligned}
    u(x,0)=\sum_{n=0}^{\infty}C_{n}\sin\left(\dfrac{n\pi x}{L}\right)=f(x),
    \end{aligned}

e,

.. math::
   :label: vxocondinit

    \begin{aligned}
    \left.\dfrac{\partial u}{\partial t}\right]_{t=0} = \sum_{n=0}^{\infty}\dfrac{n\pi c}{L}D_{n}\sin\left(\dfrac{n\pi x}{L}\right)=g(x),
    \end{aligned}

:math:`\phantom{\text{paragraph}}` Para extrair as constantes de dentro dos somatórios, recorremos à propriedade de ortogonalidade das funções seno. Com efeito, diexamos para o leitor demonstrar que:

.. math::

    \begin{aligned}
    \delta_{m,n}=\dfrac{2}{L}\int_{0}^{L}\sin\left(\dfrac{n\pi x}{L}\right)\sin\left(\dfrac{m\pi x}{L}\right)~dx,
    \end{aligned}

onde :math:`\delta_{m,n}` é a delta de Kronecker cujo valor é igual a 1 se :math:`m=n` ou 0 para :math:`m\neq n`.

:math:`\phantom{\text{paragraph}}` Multiplicando ambos os lados da :eq:`ux0condinit1` pela função :math:`\sin(m\pi x/L)`, e integrando em relação a :math:`x` podemos escrever:

.. math::

    \begin{aligned}
    \dfrac{2}{L}\int_{0}^{L}f(x)\sin\left(\dfrac{m\pi x}{L}\right)~dx=\sum_{n=0}^{\infty}C_{n}\dfrac{2}{L}\int_{0}^{L}\sin\left(\dfrac{m\pi x}{L}\right)\sin\left(\dfrac{n\pi x}{L}\right)~dx,
    \end{aligned}

ou seja,

.. math::

    \begin{aligned}
    \dfrac{2}{L}\int_{0}^{L}f(x)\sin\left(\dfrac{m\pi x}{L}\right)~dx=\sum_{n=0}^{\infty}C_{n}\delta_{m,n}=C_{m}
    \end{aligned}

e como o índice :math:`m` é arbitrário, a equação acima nos permite determinar todas as constantes :math:`C_{m}` a partir do conhecimento da forma de :math:`f(x)`, assim, escrevemos:

.. math::
   :label: intermedeq2

    \begin{aligned}
    C_{n}=\dfrac{2}{L}\int_{0}^{L}f(x)\sin\left(\dfrac{n\pi x}{L}\right)~dx,
    \end{aligned}

onde na última equação voltamos a utilizar o índice :math:`n` desde este aparece em ambos os membros da expressão e, portanto, trata-se de um índice mudo.

:math:`\phantom{\text{paragraph}}` Adotando o mesmo procedimento para remover a constante :math:`D_{n}` do interior do somatório da :eq:`vxocondinit`, podemos escrever:

.. math::
  :label: intermedeq3

    \begin{aligned}
    D_{n}=\dfrac{2}{cn\pi}\int_{0}^{L}g(x)\sin\left(\dfrac{n\pi x}{L}\right)~dx.
    \end{aligned}

.. admonition:: Sumário - Equação da Onda

    Para uma corda vibrante de comprimento :math:`L` e presa nas suas extremidades, a amplitude :math:`u(x,t)` é dada por:

    .. math::

        \begin{aligned}
        u(x,t)&=\sum_{n=0}^{\infty}\left[C_{n}\cos\left(\dfrac{n\pi ct}{L}\right)+D_{n}\sin\left(\dfrac{n\pi ct}{L}\right)\right]\sin\left(\dfrac{n\pi x}{L}\right)
        \end{aligned}

    onde

    .. math::

        \begin{aligned}
        C_{n}=\dfrac{2}{L}\int_{0}^{L}f(x)\sin\left(\dfrac{n\pi x}{L}\right)~dx,
        \end{aligned}

    e,

    .. math::

        \begin{aligned}
        D_{n}=\dfrac{2}{cn\pi}\int_{0}^{L}g(x)\sin\left(\dfrac{n\pi x}{L}\right)~dx,
        \end{aligned}

    onde :math:`f(x)` e :math:`g(x)` são a posição e velocidade da corda, respectivamente, definidos no instante inicial :math:`t=0`.


Exemplo
-------
**1.** (E. Kreyzsig, 9th, Advanced Engineering Mathematics, pg. 545) *Determine o movimento de uma corda de comprimento* :math:`L`, *inicialmente em repouso, presa em suas extremidades, e com deflexão inicial triangular:*

.. math::

    \begin{aligned}
    f(x)=\left\{
    \begin{array}{c}
    \dfrac{2k}{L}x,\qquad\text{e}\qquad 0\leq x\leq L/2\\
    \dfrac{2k}{L}(L-x),\qquad\text{e}\qquad L/2 \leq x\leq L
    \end{array}
    \right.
    \end{aligned}




.. centered:: Solução

:math:`\phantom{\text{paragraph}}` Desde que a velocidade inicial é zero, então :math:`g(x)=0` e, com isso, as constantes :math:`D_{n}=0`. Assim, resta determinar apenas as constantes :math:`C_{n}` e a solução fica reduzida à forma abaixo:

      .. math::
        :label: solucaoinicial

          \begin{aligned}
         u(x,t)=\sum_{n=0}^{\infty}C_{n}\cos\left(\dfrac{n\pi ct}{L}\right)\sin\left(\dfrac{n\pi x}{L}\right).
          \end{aligned}

:math:`\phantom{\text{paragraph}}` A determinação da constante :math:`C_{n}` é obtida por meio da :eq:`intermedeq2`, assim, segue que:

.. math::

    \begin{aligned}
    C_{n}&=\dfrac{2}{L}\int_{0}^{L}f(x)\sin\left(\dfrac{n\pi x}{L}\right)~dx\\
    &=\dfrac{2}{L}\int_{0}^{L/2}\dfrac{2k}{L}x\sin\left(\dfrac{n\pi x}{L}\right)~dx+\dfrac{2}{L}\int_{L/2}^{L}\dfrac{2k}{L}(L-x)\sin\left(\dfrac{n\pi x}{L}\right)~dx
    \end{aligned}

e fazendo a troca de variável :math:`x^{\prime}=L-x`, segue que:

.. math::

    \begin{aligned}
    C_{n}=\dfrac{2}{L}\int_{0}^{L/2}\dfrac{2k}{L}x\sin\left(\dfrac{n\pi x}{L}\right)~dx-(-1)^{n}\dfrac{2}{L}\int_{0}^{L/2}\dfrac{2k}{L}x^{\prime}\sin\left(\dfrac{n\pi x^{\prime}}{L}\right)~dx^{\prime}
    \end{aligned}

e vemos que podemos colocar a integração em evidência, visto que são idênticas. Assim, temos:

.. math::

    \begin{aligned}
    C_{n}=\left[1-(-1)^{n}\right]\dfrac{2}{L}\int_{0}^{L/2}\dfrac{2k}{L}x\sin\left(\dfrac{n\pi x}{L}\right)~dx,
    \end{aligned}

e vemos que :math:`C_{n}=0` para :math:`n` par e, assim, assumindo que :math:`n` é um número ímpar então :math:`(-1)^{n}=-1` o que nos permite escrever:

.. math::

    \begin{aligned}
    C_{n}=\dfrac{4}{L}\dfrac{2k}{L}\int_{0}^{L/2}x\sin\left(\dfrac{n\pi x}{L}\right)~dx=\dfrac{8k}{L^{2}}\int_{0}^{L/2}x\sin\left(\dfrac{n\pi x}{L}\right)~dx.
    \end{aligned}


:math:`\phantom{\text{paragraph}}` Para resolver a integração restante, vamos fazer uma nova troca de variável :math:`y=n\pi x/L`, assim,

.. math::

    \begin{aligned}
    C_{n}=\dfrac{8k}{L^{2}}\dfrac{L^{2}}{n^{2}\pi^{2}}\int_{0}^{n\pi/2}y\sin y~dy=-\dfrac{8k}{n^{2}\pi^{2}}\left[\dfrac{d}{d\beta}\int_{0}^{n\pi/2}\cos(\beta y)~dy\right]_{\beta\rightarrow 1}
    \end{aligned}

onde, no último passo nos utilizamos da técnica de Feynman para simplificar a integração. Assim, segue que:

.. math::

    \begin{aligned}
    C_{n}=\dfrac{8k}{n^{2}\pi^{2}}\sin\left(\dfrac{n\pi}{2}\right).
    \end{aligned}

:math:`\phantom{\text{paragraph}}` Substituindo a constante :math:`C_{n}` na equação para :math:`u(x,t)`, obtemos o resultado final:

.. math::

    \begin{aligned}
    u(x,t)=\sum_{n=0}^{\infty}\dfrac{8k}{n^{2}\pi^{2}}\sin\left(\dfrac{n\pi}{2}\right)\cos\left(\dfrac{n\pi         ct}{L}\right)\sin\left(\dfrac{n\pi x}{L}\right).
        \end{aligned}
