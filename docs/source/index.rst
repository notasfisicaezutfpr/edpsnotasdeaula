Notas de Aula de Métodos Matemáticos
====================================

Aqui faremos o compartilhamento dos apontamentos da disciplina de Métodos Matemáticos tratados nas aulas de terça-feira e também relacionados com o conteúdo compartilhado na plataforma Moodle. Use o índice abaixo para acessar as seções deste documento.


.. toctree::
   :maxdepth: 4
   :caption: Conteúdo
   :numbered:
   
   aula_1
   aula_2
   aula_3
   aula_4
